{
    //No.1 - Mengubah fungsi menjadi fungsi arrow

const golden = () => {
    console.log("this is golden!!")
}
    golden()

}

{
    //No.2 - Sederhanakan menjadi Object literal di ES6

const newFunction = (firstName, lastName)=>{
    return {
    firstName: firstName,
    lastName: lastName,
    fullName: ()=>{
    console.log(firstName + " " + lastName)
    return
    }
    }
    }
    //Drive Code
    newFunction("William", "Imoh").fullName()


}


{
//No.3 - Destructuring

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
    }

    const   {firstName, lastName, destination, occupation,spell}= newObject
    console.log(newObject)
    console.log("")

}




{

    //No.4 - Array Spreading

    const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east ]
//Driver Code
console.log(combined)
console.log("")



}


{
    //No.5 - Template Literals

    const planet = "earth"
const view = "glass"
var before = `Lorem ` + `view` + `dolor sit amet,`  +
`consectetur adipiscing elit,` + `planet` + `do eiusmod tempor ` +
`incididunt ut labore et dolore magna aliqua. Ut enim` +
` ad minim veniam`
// Driver Code
console.log(before) 



}

