//No. 1 - Animal Class

class Animal {
    constructor(name) {
        this._animalName = name;
        this._animalLegs = 4;
        this._animalCold_blooded = false;
    } get name() {
        return this._animalName;
    } get legs() {
        return this._animalLegs;
    } get cold_blooded() {
        return this._animalCold_blooded;
    } set name(x) {
        this.animalName = x;
    } set legs(y) {
        this._animalLegs = y;
    } set cold_blooded(z) {
        this._animalCold_blooded = false;
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name)
        this._animalName = name;
        this._animalLegs = 2;
    }
    yell() { return console.log("Auooo"); }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this._animalName = name;
    }
    jump() { return console.log("hop hop"); }
}

let sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log("");

let sungokong = new Ape("kera sakti")
console.log(sungokong.name); //"kera sakti"
console.log(sungokong.legs); //2
console.log(sungokong.cold_blooded) // false
sungokong.yell() // "Auooo"
console.log("");

let kodok = new Frog("buduk")
console.log(kodok.name); //buduk
console.log(kodok.legs); //4
console.log(kodok.cold_blooded) // false
kodok.jump() // "hop hop"
console.log("");

// //02.Function to Class

class Clock{
constructor({ template }) {
    this.template = template
}
    
    render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
    .replace('h', hours)
    .replace('m', mins)
    .replace('s', secs);
    console.log(output);
    }
    stop() {
    clearInterval(this.timer);
    }
    start () {
    this.render();
    this.timer = setInterval(()=>this.render(), 1000);
    
    }
    }
    var clock = new Clock({template: 'h:m:s'});
    clock.start();


