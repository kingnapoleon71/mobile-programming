import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground,Image, TextInput,SafeAreaView , TouchableOpacity} from 'react-native';


export default function App()  {
  return (
    <View style={styles.container}>
    <ImageBackground source={require('./../images/background.png')}
    style={{width: '100%',flex:1 }}>
      <Image style={styles.Logo} source={require('./../images/Profile.png')}/>
      <Text style={styles.title}>Biodata</Text>
      <Text style={styles.data}>Nama</Text>
      <Text style={styles.nama}>Dionisius Chandra Irawan</Text>
      <Text style={styles.kelas}>Kelas</Text>
      <Text style={styles.kelas2}>Informatika Pagi A</Text>
      <Text style={styles.prodi}>Program Studi</Text>
      <Text style={styles.prodi2}>Teknik Informatika</Text>
      <Image style={styles.sosmed} source={require('./../images/instagram.png')}/>
      <Text style={styles.sosmed2}>yon_dion31</Text>
      <Image style={styles.kiri} source={require('./../images/Kiri.png')}/>
      <Image style={styles.kanan} source={require('./../images/Kanan.png')}/>

      </ImageBackground>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Logo:{
    position : "absolute",
    top:100,
    width: 200,
    height: 200

  },
  title:{
    position : "absolute",
    left: 260,
     top:70,
    color : 'blue',
    fontSize: 36,
    backgroundColor:'#1e9eaf',
    borderRadius: 15,
  },
  data:{
    position : "absolute",
    left: 100,
     top:350,
    color : 'black',
    fontSize: 12,
    backgroundColor:'#c4c4c4',
    width: 270,
    height: 238,
  },
  nama:{
    position : "absolute",
    left: 100,
    top:365,
   color : 'black',
    fontSize: 20.5,
  },
  kelas:{
    position : "absolute",
    left: 100,
    top:400,
   color : 'black',
    fontSize: 12,
  },
  kelas2:{
    position : "absolute",
    left: 100,
    top:415,
   color : 'black',
    fontSize: 20.5,
  },
  prodi:{
    position : "absolute",
    left: 100,
    top:455,
   color : 'black',
    fontSize: 12,
  },
  prodi2:{
    position : "absolute",
    left: 100,
    top:470,
   color : 'black',
    fontSize: 20.5,
  },
  sosmed:{
    position : "absolute",
    left: 120,
    top:520,
    width: 50,
    height: 50,
  },
  sosmed2:{
    position : "absolute",
    left: 180,
    top:525,
   color : 'black',
    fontSize: 20.5,
  },
  kiri:{
    position : "absolute",
    left: 30,
    top:800,
    width: 50,
    height: 50,
  },
  kanan:{
    position : "absolute",
    right: 30,
    top:800,
    width: 50,
    height: 50,
  },
});