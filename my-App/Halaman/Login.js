import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground,Image, TextInput,SafeAreaView , TouchableOpacity} from 'react-native';



export default function App() {
  const [text, onChangeText] = React.useState();
  const [Password, onChangePassword] = React.useState();

  return (
    <View style={styles.container}>
    <ImageBackground source={require('./../images/background.png')}
    style={{width: '100%',flex:1 }}>
      <View style={styles.p}>
      <Text style={styles.title}>Amigo's</Text>
      <Image style={styles.Logo} source={require('./../images/Logo.png')}/>
      </View>
      <SafeAreaView>
              <TextInput style={styles.input} onChangeText={onChangeText} value={text}>
              </TextInput>
            </SafeAreaView>
            
            <SafeAreaView>
              <TextInput style={styles.input} onChangeText={onChangePassword} value={Password} secureTextEntry={true}>
              </TextInput>
              <View style={styles.p}>

              <TouchableOpacity style={styles.Login}>
              
              </TouchableOpacity>
              
              </View> 
              </SafeAreaView>
              <Text style={styles.q}>Login</Text>
              
              <Text style={styles.kanan}>Don't have an Account?</Text>
              <Text style={styles.kiri}>Forget The Password?</Text>
               
               
               <Image style={styles.Log2} source={require('./../images/password.png')}/>
                <Image style={styles.Log} source={require('./../images/email.png')}/>
      </ImageBackground>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title:{
  position : "absolute",
  top:150,
  color : 'white',
  fontSize: 36,
  alignItems: 'center',
},
  Logo:{
  position : "absolute",
  top:250,
  width: 100,
  height: 100
},
input: {
  top: 500,
  height: 60,
  margin: 12,
  borderWidth: 5,
  color : 'white',
  backgroundColor: '#808080',
  paddingLeft: 105,
  
},
Sign: {
  top: 165,
  left:10,
  color : 'white',
  
},
Password: {
  top:500,
  left:20,
  color : 'white',
  
},
Login: {
  top: 495,
  width: 120,
  height: 60,
  color : 'black',
  fontSize: 33,
  backgroundColor: '#ffffff',
  borderRadius: 30,
  borderWidth: 5,
  borderColor: '#ff0080',
},
p:{
  
  alignItems: 'center',
},
Log:{
  position : "absolute",
  top:515,
  left: 25,
  width: 50,
  height: 50
},
Log2:{
  position : "absolute",
  top:603,
  left:25,
  width: 45,
  height: 45
},
q:{
  position : "absolute",
  top:617,
  left:117,
  
  margin: 50,
  color : 'black',
  fontSize: 30,
},
kiri:{
  position : "absolute",
  left: 30,
  top:830,
  color : 'white',
  fontSize: 12.5,

},
kanan:{
  position : "absolute",
  right: 30,
  top:830,
  color : 'white',
  fontSize: 15.5,
},
});

